import urllib.request
import json, math, sys, os, csv
from bs4 import BeautifulSoup
from bs4.element import Comment


def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True


def read_article_url(url):
    # print("Reading From: ", url)
    html_doc = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(html_doc, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)
    return u" ".join(t.strip() for t in visible_texts)


def parse_page_results(results_page, page_number):
    page_results = []
    for article in results_page:
        result = {}
        source = article['source']['name']
        title = article['title']
        url = article['url']
        publish_dt = article['publishedAt']

        result['source'] = source
        result['title'] = title
        result['url'] = url
        result['publishedAt'] = publish_dt
        result['page_number'] = page_number
        try:
            result['content'] = read_article_url(url)
        except (urllib.error.HTTPError, urllib.error.URLError) as e:
            print("Failed to Read: ", url)
            continue

        page_results.append(result)

    return page_results


def write_results_to_file(results, filename):
    with open(filename, 'w') as file:
        file.write(json.dumps(results))


if __name__ == '__main__':

    rel_path = os.path.abspath(os.path.dirname(__file__))
    output_path = os.path.join(rel_path, "../data/articles/")

    page_size = 100
    max_pages = 5
    keywords = {'+Tesla+car', '+Ford+car', '+General+Motors+car', '+Toyota+car', '+Volkswagen+car'}

    # For Later - word sentiments definitions
    # https://hlt-nlp.fbk.eu/technologies/sentiwords

    news_api_url = ('https://newsapi.org/v2/everything?'
                    'q=+{keyword}&'
                    'language=en&'
                    'sortBy=popularity&'
                    'pageSize={pageSize}&'
                    'page={page_number}&'
                    'apiKey=9d1c3527810f48879ed7a73aaa11df10')

    for keyword in keywords:
        page_number = 1
        parsed_results = []
        while page_number <= max_pages:
            response = urllib.request.urlopen(news_api_url.format(keyword=keyword, pageSize=page_size, page_number=page_number))
            content = json.loads(response.read().decode('utf-8'))

            total_articles = content['totalResults']
            pages_returned = math.ceil(total_articles / page_size)
            page_of_json = content['articles']

            parsed_page = parse_page_results(page_of_json, page_number)
            parsed_results.extend(parsed_page)

            print("Parsed %s, Page %i" % (keyword, page_number))

            max_pages = min(max_pages, pages_returned)
            page_number += 1

        print('Writing %s Results to File' % keyword)
        write_results_to_file(parsed_results, output_path+keyword+".csv")

